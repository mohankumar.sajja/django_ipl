from django import forms
from charts.models import Match

class MatchForm(forms.ModelForm):
   class Meta:
      model = Match
      fields = '__all__'