from django.db import models
from postgres_copy import CopyManager

# Create your models here.

class Match(models.Model):
    season = models.PositiveIntegerField(null=True)
    city = models.CharField(max_length=100, null=True)
    date = models.DateField(null=True)
    team1 = models.CharField(max_length=128, null=True)
    team2 = models.CharField(max_length=128, null=True)
    toss_winner = models.CharField(max_length=128, null=True)
    toss_decision = models.CharField(max_length=128, null=True)
    result = models.CharField(max_length=128, null=True)
    dl_applied = models.PositiveSmallIntegerField(null=True)
    winner = models.CharField(max_length=128, null=True)
    win_by_runs = models.PositiveSmallIntegerField(null=True)
    win_by_wickets = models.PositiveSmallIntegerField(null=True)
    player_of_match = models.CharField(max_length=128, null=True)
    venue = models.CharField(max_length=200, null=True)
    umpire1 = models.CharField(max_length=100, null=True)
    umpire2 = models.CharField(max_length=100, null=True)
    umpire3 = models.CharField(max_length=100, null=True)
    objects = CopyManager()

    class Meta:
        ordering = ('-date',)

    def __repr__(self):
        return f'{self.season} Match Between {self.team1} vs {self.team2}'

class Delivery(models.Model):
    match_id = models.ForeignKey(Match, on_delete=models.CASCADE)
    inning = models.PositiveSmallIntegerField(null=True)
    batting_team = models.CharField(max_length=128, null=True)
    bowling_team = models.CharField(max_length=128, null=True)
    over = models.PositiveSmallIntegerField(null=True)
    ball = models.PositiveSmallIntegerField(null=True)
    batsman = models.CharField(max_length=128, null=True)
    non_striker = models.CharField(max_length=128, null=True)
    bowler = models.CharField(max_length=128, null=True)
    is_super_over = models.PositiveSmallIntegerField(null=True)
    wide_runs = models.PositiveSmallIntegerField(null=True)
    bye_runs = models.PositiveSmallIntegerField(null=True)
    legbye_runs = models.PositiveSmallIntegerField(null=True)
    noball_runs = models.PositiveSmallIntegerField(null=True)
    penalty_runs = models.PositiveSmallIntegerField(null=True)
    batsman_runs = models.PositiveSmallIntegerField(null=True)
    extra_runs = models.PositiveSmallIntegerField(null=True)
    total_runs = models.PositiveSmallIntegerField(null=True)
    player_dismissed = models.CharField(max_length=128, null=True)
    dismissal_kind = models.CharField(max_length=128, null=True)
    fielder = models.CharField(max_length=128, null=True)
    objects = CopyManager()

    def __repr__(self):
        return f'{self.match_id} {self.over} {self.ball}'

    