from django import forms
from charts.models import Delivery

class DeliveriesForm(forms.ModelForm):
    class Meta:
        model = Delivery
        fields = '__all__'