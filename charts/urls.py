from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('matches_per_year/', views.matches_per_year_template, name='matches_per_year_template'),
    path('matches_per_year/data/', views.matches_per_year_chart_data, name='matches_per_year_chart_data'),  path('matches_won_by_team/', views.matches_won_by_teams, name='matches_won_by_teams'),
    path('matches_won_by_team/data/', views.matches_won_by_teams_chart_data, name='matches_won_by_teams_chart_data'),
    path('extra_runs/', views.extra_runs, name='extra_runs'),
    path('extra_runs/data/', views.extra_runs_chart_data, name='extra_runs_chart_data'),
    path('economical_bowlers/', views.economical_bowlers, name='economical_bowlers'),
    path('economical_bowlers/data/', views.economical_bowlers_chart_data, name='economical_bowlers_chart_data'),
    # DJANGO HARDCODED API VIEWS
    path('api/matches/', views.matches_api, name='matches_api'),
    path('api/matches/<int:pk>/', views.match_api, name='match_api'),
    path('api/deliveries/', views.deliveries_api, name='deliveries_api'),
    path('api/deliveries/<int:pk>/', views.delivery_api, name='delivery_api'),
    # path('matches/', views.match_list, name='matches'),
    # REST API Data
    path('drf_api/matchess/', views.match_list, name='match_list'),
    path('drf_api/matches/<int:pk>/', views.match_detail, name='match_detail'),
    path('drf_api/deliveries/', views.delivery_list, name='delivery_list'),
    path('drf_api/deliveries/<int:pk>/', views.delivery_detail, name='delivery_detail'),
]