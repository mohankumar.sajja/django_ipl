import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Match, Delivery
from django.db.models import Count, Sum, FloatField, Case, When
from django.db.models.functions import Cast
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework import status
from charts.serializers import ChartSerializer, DeliverySerializer
from charts.store_matches import MatchForm
from charts.store_deliveries import DeliveriesForm
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
# DJANGO API Imports
from django.core import serializers
import json

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# Create your views here.
@cache_page(CACHE_TTL)
def index(request):
    return render(request, 'charts/index.html')

@cache_page(CACHE_TTL)
def matches_per_year_template(request):
    return render(request, 'charts/matches_per_year.html')

@cache_page(CACHE_TTL)
def matches_per_year_chart_data(request):
    data = Match.objects.all().values('season').annotate(count=Count('season')).order_by('season')
    years = [d['season'] for d in data]
    total_matches = [d['count'] for d in data]
    
    chart = {
        'chart': {'renderTo': 'container',
            'type': 'column'},
        'title': {'text': 'Matches per Year'},
        'xAxis': {
            'categories': years,
            'title': {
                'text': 'Years'
            }
        },
        'series': [{
            'data': total_matches
        }],
        'yAxis': {
            'min': 0,
            'title': {
                'text': 'Matches'
            },
            'tickInterval': 5
        }

    }

    return JsonResponse(chart)

def matches_won_by_teams(request):
    return render(request, 'charts/matches_won_by_teams.html')

def matches_won_by_teams_chart_data(request):
    data = Match.objects.all().values('season', 'winner').annotate(count=Count('winner')).order_by('season', 'winner')
    years = []
    [years.append(d['season']) for d in data if d['season'] not in years]
    teams_data = {}
    
    for row in data:
        if row['winner'] not in teams_data:
            dic = teams_data[row['winner']] = {}
            for season in years:
                dic[str(season)] = 0
        teams_data[row['winner']][str(row['season'])] = row['count']
    series = []
    for team in teams_data:
        temp_dic = {'data': list(teams_data[team].values()), 'name':team}
        series.append(temp_dic)
    print(series)
    
    chart = {
        'chart': {
        'type': 'bar'
        },
        'title': {
            'text': 'Stacked bar chart'
        },
        'xAxis': {
            'categories': years
        },
        'yAxis': {
            'min': 0,
            'title': {
                'text': 'Matches Won'
            }
        },
        'legend': {
            'reversed': 'true'
        },
        'plotOptions': {
            'series': {
                'stacking': 'normal'
            }
        },
        'series': series

    }

    return JsonResponse(chart)

def extra_runs(request):
    return render(request, 'charts/extra_runs.html')

def extra_runs_chart_data(request):
    data = Delivery.objects.select_related('match_id').filter(match_id__season=2016).values_list('bowling_team').annotate(extras=Sum('extra_runs'))
    teams = [d[0] for d in data]
    extras = [d[1] for d in data]
    
    chart = {
        'chart': {'renderTo': 'container',
            'type': 'column'},
        'title': {'text': 'EXTRA RUNS BY EACH TEAM - 2016'},
        'xAxis': {
            'categories': teams,
            'title': {
                'text': 'TEAM NAMES'
            }
        },
        'series': [{
            'data': extras
        }],
        'yAxis': {
            'min': 0,
            'title': {
                'text': 'EXTRA RUNS'
            },
            'tickInterval': 5
        }

    }

    return JsonResponse(chart)

def economical_bowlers(request):
    return render(request, 'charts/economical_bowlers.html')

def economical_bowlers_chart_data(request):
    data = Delivery.objects.select_related('match_id').filter(match_id__season=2015).values_list('bowler').annotate(economy=Cast((Sum('total_runs')-Sum('bye_runs')-Sum('legbye_runs')-Sum('penalty_runs')) / (Count(Case(When(noball_runs=0, wide_runs=0, then=1),default=0, output_field=FloatField())) / 6.0), FloatField())).order_by('economy')[:10]
    bowler = [d[0] for d in data]
    economy = [d[1] for d in data]
    
    chart = {
        'chart': {'renderTo': 'container',
            'type': 'column'},
        'title': {'text': 'Top 10 Economical Bowlers - 2015'},
        'xAxis': {
            'categories': bowler,
            'title': {
                'text': 'Bowler Name'
            }
        },
        'series': [{
            'data': economy
        }],
        'yAxis': {
            'min': 0,
            'title': {
                'text': 'Economy'
            },
            'tickInterval': 0.2
        }

    }

    return JsonResponse(chart)

### DJANGO API ###

# MATCH API
@csrf_exempt
def matches_api(request):
    """
    List all matches, or create a new match.
    """
    matches = Match.objects.all()
    if request.method == 'GET':
        data = {"results": list(matches.values())}
        return JsonResponse(data)

    elif request.method == 'POST':
        form = MatchForm(request.POST, request.data)
        if form.is_valid():
            form.save() 
            return JsonResponse(form.data, status=201) 
        return JsonResponse(form.errors.as_json(), status=400)
        # body = json.loads(request.body.decode('utf-8'))
        # season = body.get('season')
        # city = body.get('city')
        # date = body.get('date')
        # team1 = body.get('team1')
        # team2 = body.get('team2')
        # toss_winner = body.get('toss_winner')
        # toss_decision = body.get('toss_decision')
        # result = body.get('result')
        # dl_applied = body.get('dl_applied')
        # winner = body.get('winner')
        # win_by_runs = body.get('win_by_runs')
        # win_by_wickets = body.get('win_by_runs')
        # player_of_match = body.get('player_of_match')
        # venue = body.get('venue')
        # umpire1 = body.get('umpire1')
        # umpire2 = body.get('umpire2')
        # umpire3 = body.get('umpire3')
        

@csrf_exempt
def match_api(request, pk):
    try:
        match = Match.objects.values().get(pk=pk)
    except Match.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        # return JsonResponse(match)
        match_serializer = MatchForm(match)
        return JsonResponse(match_serializer.data)

    elif request.method == 'PUT':
        form = MatchForm(request.POST, request.data)
        if form.is_valid():
            form.save() 
            return JsonResponse(form.data, status=201) 
        return JsonResponse(form.errors.as_json(), status=400)

    elif request.method == 'DELETE':
        match.delete() 
        return HttpResponse(status=204)

# DELIVERY API
@csrf_exempt
def deliveries_api(request):
    """
    List all deliveries, or create new delivery.
    """
    deliveries = Delivery.objects.select_related('match_id').all()[:1000]
    if request.method == 'GET':
        data = {"results": list(deliveries.values())}
        return JsonResponse(data)
    elif request.method == 'POST':
        form = DeliveriesForm(request.POST, request.data)
        if form.is_valid():
            form.save() 
            return JsonResponse(form.data, status=201) 
        return JsonResponse(form.errors.as_json(), status=400)

@csrf_exempt
def delivery_api(request, pk):
    try:
        delivery = Delivery.objects.select_related('match_id').values().get(pk=pk)
    except Delivery.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        delivery_serializer = DeliveriesForm(delivery)
        return JsonResponse(delivery_serializer.data)
    
    elif request.method == 'PUT':
        form = DeliveriesForm(request.POST, request.data)
        if form.is_valid():
            form.save() 
            return JsonResponse(form.data, status=201) 
        return JsonResponse(form.errors.as_json(), status=400)

    elif request.method == 'DELETE':
        delivery.delete() 
        return HttpResponse(status=204)

### REST API

@csrf_exempt
def match_list(request):
    if request.method == 'GET':
        matches = Match.objects.all()
        matches_serializer = ChartSerializer(matches, many=True)
        return JsonResponse(matches_serializer.data, safe=False)

    elif request.method == 'POST':
        match_data = JSONParser().parse(request)
        match_serializer = ChartSerializer(data=match_data)
        if match_serializer.is_valid():
            match_serializer.save() 
            return JsonResponse(match_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(match_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@csrf_exempt
def match_detail(request, pk):
    try:
        match = Match.objects.get(pk=pk)
    except Match.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        match_serializer = ChartSerializer(match)
        return JsonResponse(match_serializer.data)
    
    elif request.method == 'PUT': 
        match_data = JSONParser().parse(request) 
        match_serializer = ChartSerializer(match, data=match_data) 
        if match_serializer.is_valid(): 
            match_serializer.save() 
            return JsonResponse(match_serializer.data) 
        return JsonResponse(match_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        match.delete() 
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
def delivery_list(request):
    if request.method == 'GET':
        deliveries = Delivery.objects.select_related('match_id').all()
        deliveries_serializer = DeliverySerializer(deliveries, many=True)
        return JsonResponse(deliveries_serializer.data, safe=False)

    elif request.method == 'POST':
        deliveries_data = JSONParser().parse(request)
        deliveries_serializer = DeliverySerializer(data=deliveries_data)
        if deliveries_serializer.is_valid():
            deliveries_serializer.save() 
            return JsonResponse(deliveries_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(deliveries_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@csrf_exempt
def delivery_detail(request, pk):
    try:
        delivery = Delivery.objects.select_related('match_id').get(pk=pk)
    except Delivery.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        delivery_serializer = DeliverySerializer(delivery)
        return JsonResponse(delivery_serializer.data)
    
    elif request.method == 'PUT': 
        delivery_data = JSONParser().parse(request) 
        delivery_serializer = DeliverySerializer(delivery, data=delivery_data) 
        if delivery_serializer.is_valid(): 
            delivery_serializer.save() 
            return JsonResponse(delivery_serializer.data) 
        return JsonResponse(delivery_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        delivery.delete() 
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


