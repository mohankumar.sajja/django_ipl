from rest_framework import serializers
from charts.models import Match, Delivery

class ChartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = '__all__'
    
class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = '__all__'
